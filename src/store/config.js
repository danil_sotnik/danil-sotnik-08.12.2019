import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import allReducers from './reducers/combine';

const middlewares = [thunk];
if (process.env.NODE_ENV === `development`) {
  middlewares.push(logger);
}

export const store = createStore(
  allReducers,
  composeWithDevTools(applyMiddleware(...middlewares)),
);
