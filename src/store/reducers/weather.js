import {
  convertISOtoDay,
  convertFahrenheitToCelsius,
  convertCelsiusToFahrenheit,
} from '../../services/helper';
import { GET_WEATHER_CITY, CONVERT_TEMPERATURE } from '../actions/constants';

const initialState = {
  city: null,
  daily: null,
  isCelsius: false,
};

const getWeather = (state, payload) => {
  const { city, daily } = payload;
  const newCity = {
    temp: city.Temperature.Imperial.Value,
    weather: city.WeatherText,
    city: city.name,
  };
  const newForecast = daily.map((item) => ({
    day: convertISOtoDay(item.Date),
    temp: {
      min: item.Temperature.Minimum.Value,
      max: item.Temperature.Maximum.Value,
    },
  }));
  return {
    ...state,
    city: newCity,
    daily: newForecast,
    isCelsius: false,
  };
};

const convertTemperature = (state, payload) => {
  if (payload) {
    return {
      ...state,
      isCelsius: true,
      city: {
        ...state.city,
        temp: convertFahrenheitToCelsius(state.city.temp),
      },
      daily: state.daily.map((value) => ({
        ...value,
        temp: {
          min: convertFahrenheitToCelsius(value.temp.min),
          max: convertFahrenheitToCelsius(value.temp.max),
        },
      })),
    };
  }
  return {
    ...state,
    isCelsius: false,
    city: {
      ...state.city,
      temp: convertCelsiusToFahrenheit(state.city.temp),
    },
    daily: state.daily.map((value) => ({
      ...value,
      temp: {
        min: convertCelsiusToFahrenheit(value.temp.min),
        max: convertCelsiusToFahrenheit(value.temp.max),
      },
    })),
  };
};

export default function settings(state = initialState, { type, payload }) {
  switch (type) {
    case GET_WEATHER_CITY:
      return getWeather(state, payload);
    case CONVERT_TEMPERATURE:
      return convertTemperature(state, payload);
    default:
      return state;
  }
}
