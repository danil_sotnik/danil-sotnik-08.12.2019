import { SET_ERROR, FINISH_REQUEST, BEGIN_REQUEST, SET_THEME } from "../actions/constants";

const themeValue = localStorage.getItem('theme');

const initialState = {
  theme: themeValue ? themeValue : 'light',
  isLoading: false,
  error: '',
};

const setTheme = (state) => {
  const isLight = state.theme === 'light';
  return {
    ...state,
    theme: isLight ? 'dark' : 'light',
  };
};

const beginRequest = (state) => ({
  ...state,
  isLoading: true,
});

const finishRequest = (state) => ({
  ...state,
  isLoading: false,
});

const setError = (state, payload) => ({
  ...state,
  error: payload,
  isLoading: false,
});

export default function settings(state = initialState, { type, payload }) {
  switch (type) {
    case SET_THEME:
      return setTheme(state);
    case BEGIN_REQUEST:
      return beginRequest(state);
    case FINISH_REQUEST:
      return finishRequest(state);
    case SET_ERROR:
      return setError(state, payload);
    default:
      return state;
  }
}
