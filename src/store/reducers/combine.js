import { combineReducers } from 'redux';
import search from './search';
import settings from './settings';
import weather from './weather';
import favourites from './favourites';

const allReducers = combineReducers({
  settings,
  search,
  weather,
  favourites,
});

export default allReducers;
