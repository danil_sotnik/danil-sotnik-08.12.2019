import { FIND_CITY } from "../actions/constants";

const initialState = {
  suggestOptions: [],
};

const findCity = (state, payload) => {
  const newItems = payload.map((item) => item.LocalizedName).slice(0, 5);
  return {
    ...state,
    suggestOptions: newItems,
  };
};

export default function settings(state = initialState, { type, payload }) {
  switch (type) {
    case FIND_CITY:
      return findCity(state, payload);
    default:
      return state;
  }
}
