import { SET_FAVOURITES, UPDATE_FAVOURITES_DATA } from '../actions/constants';

const initialData = JSON.parse(localStorage.getItem('favourites'));

const initialState = {
  cities: initialData || [],
};

const changeFavouriteStatusAction = (state, payload) => {
  const isAlreadySaved =
    state.cities.filter((item) => item.Key === payload.Key).length !== 0;

  const newItems = isAlreadySaved
    ? state.cities.filter((item) => item.Key !== payload.Key)
    : [...state.cities, payload];
  localStorage.setItem('favourites', JSON.stringify(newItems));
  return {
    ...state,
    cities: newItems,
  };
};

const updateFavouritesData = (state, payload) => {
  localStorage.setItem('favourites', JSON.stringify(payload));
  return {
    ...state,
    cities: payload,
  };
};

export default function favourites(state = initialState, { type, payload }) {
  switch (type) {
    case SET_FAVOURITES:
      return changeFavouriteStatusAction(state, payload);
    case UPDATE_FAVOURITES_DATA:
      return updateFavouritesData(state, payload);
    default:
      return state;
  }
}
