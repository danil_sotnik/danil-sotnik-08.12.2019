import {
  SET_SEARCH_VALUE,
  SELECT_SEARCH_VALUE,
  SEARCH_CHANGE_VALUE,
  CLOSE_OPTIONS_LIST,
} from '../actions/constants';

const initialState = {
  suggestOptions: [],
  selectedValue: {
    Version: 1,
    Key: '215854',
    Type: 'City',
    Rank: 31,
    LocalizedName: 'Tel Aviv',
  },
  inputValue: '',
};

const findCity = (state, payload) => ({
  ...state,
  suggestOptions: payload.slice(0, 5).map((item) => {
    const { Country, AdministrativeArea, ...rest } = item;
    return rest;
  }),
});

const searchChangeValue = (state, payload) => {
  const processedValues = {};
  if (!payload) processedValues.suggestOptions = [];
  return {
    ...state,
    ...processedValues,
    inputValue: payload,
  };
};
const closeOptionsList = (state) => ({
  ...state,
  suggestOptions: [],
});

const selectValue = (state, payload) => ({
  ...state,
  selectedValue: payload,
  suggestOptions: [],
  inputValue: payload.LocalizedName,
});

export default function settings(state = initialState, { type, payload }) {
  switch (type) {
    case SET_SEARCH_VALUE:
      return findCity(state, payload);
    case SELECT_SEARCH_VALUE:
      return selectValue(state, payload);
    case SEARCH_CHANGE_VALUE:
      return searchChangeValue(state, payload);
    case CLOSE_OPTIONS_LIST:
      return closeOptionsList(state);
    default:
      return state;
  }
}
