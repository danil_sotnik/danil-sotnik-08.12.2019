import { createSelector } from 'reselect';

export const settingSelector = (state) => state.settings;

export const themeSelector = createSelector(
  [settingSelector],
  (settings) => settings.theme,
);

export const loadingSelector = createSelector(
  [settingSelector],
  (settings) => settings.isLoading,
);

export const errorSelector = createSelector(
  [settingSelector],
  (settings) => settings.error,
);
