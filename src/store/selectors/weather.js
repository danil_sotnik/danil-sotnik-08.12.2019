import { createSelector } from 'reselect';

export const weatherSelector = (state) => state.weather;

export const weatherCitySelector = createSelector(
  [weatherSelector],
  (weather) => weather.city,
);

export const weatherForecastSelector = createSelector(
  [weatherSelector],
  (weather) => weather.daily,
);

export const isCelsiusSelector = createSelector(
  [weatherSelector],
  (weather) => weather.isCelsius,
);
