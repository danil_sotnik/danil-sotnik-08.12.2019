import { createSelector } from 'reselect';

export const search = (state) => state.search;

export const searchSelector = createSelector(
  [search],
  (item) => item.suggestOptions,
);

export const searchInputSelector = createSelector(
  [search],
  (item) => item.inputValue,
);

export const searchSelectedSelector = createSelector(
  [search],
  (item) => item.selectedValue,
);
