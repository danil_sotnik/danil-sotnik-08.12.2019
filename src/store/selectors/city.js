import { createSelector } from 'reselect';

export const citySelector = (state) => state.city;

export const searchSelector = createSelector(
  [citySelector],
  (city) => city.suggestOptions,
);
