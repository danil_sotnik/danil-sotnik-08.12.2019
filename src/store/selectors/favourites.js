import { createSelector } from 'reselect';

export const favourites = (state) => state.favourites;

export const favouritesSelector = createSelector(
  [favourites],
  (item) => item.cities || [],
);
