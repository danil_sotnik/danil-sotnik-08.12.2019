import { apiFindCity } from '../../api/city';
import { FIND_CITY } from './constants';

export const findCityAction = (data) => ({
  type: FIND_CITY,
  payload: data,
});

export const findCityFromSearch = (value) => async (dispatch) => {
  try {
    const { data } = await apiFindCity(value);
    const payload = data === '' ? [] : data;
    dispatch(findCityAction(payload));
  } catch (error) {}
};
