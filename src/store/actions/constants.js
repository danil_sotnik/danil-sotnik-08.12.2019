//City
export const FIND_CITY = 'FIND_CITY';

//Search
export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
export const SELECT_SEARCH_VALUE = 'SELECT_SEARCH_VALUE';
export const SEARCH_CHANGE_VALUE = 'SEARCH_CHANGE_VALUE';
export const CLOSE_OPTIONS_LIST = 'CLOSE_OPTIONS_LIST';

//Favourites
export const SET_FAVOURITES = 'SET_FAVOURITES';
export const UPDATE_FAVOURITES_DATA = 'UPDATE_FAVOURITES_DATA';

//Settings
export const SET_THEME = 'SET_THEME';
export const SET_ERROR = 'SET_ERROR';
export const BEGIN_REQUEST = 'BEGIN_REQUEST';
export const FINISH_REQUEST = 'FINISH_REQUEST';

//Weather
export const GET_WEATHER_CITY = 'GET_WEATHER_CITY';
export const CONVERT_TEMPERATURE = 'CONVERT_TEMPERATURE';
