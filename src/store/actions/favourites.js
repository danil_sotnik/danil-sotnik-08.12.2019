import { store } from '../config';
import { favouritesSelector } from '../selectors/favourites';
import { apiGetWeatherForCity } from '../../api/weather';
import { beginRequestAction, finishRequestAction } from './settings';
import { SET_FAVOURITES, UPDATE_FAVOURITES_DATA } from './constants';

export const changeFavouriteStatusAction = () => ({
  type: SET_FAVOURITES,
  payload: {
    ...store.getState().search.selectedValue,
    temp: store.getState().weather.city.temp,
    weather: store.getState().weather.city.weather,
  },
});

export const updateFavouritesDataAction = (payload) => ({
  type: UPDATE_FAVOURITES_DATA,
  payload,
});

export const getAllFavouritesWeather = () => async (dispatch, getState) => {
  try {
    dispatch(beginRequestAction());
    const state = getState();
    const favourites = favouritesSelector(state);

    const requests = favourites.map(async (item) => {
      const { data } = await apiGetWeatherForCity(item.Key);
      return {
        ...item,
        temp: data[0].Temperature.Imperial.Value,
        weather: data[0].WeatherText,
      };
    });

    const weather = await Promise.all(requests);
    dispatch(updateFavouritesDataAction(weather));
    dispatch(finishRequestAction());
  } catch (error) {}
};
