import {
  SET_THEME,
  SET_ERROR,
  BEGIN_REQUEST,
  FINISH_REQUEST,
} from './constants';

export const setThemeAction = () => ({
  type: SET_THEME,
});

export const setErrorState = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const beginRequestAction = () => ({
  type: BEGIN_REQUEST,
});

export const finishRequestAction = () => ({
  type: FINISH_REQUEST,
});
