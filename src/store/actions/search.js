import { apiSearchCity, apiSearchCityByLocation } from '../../api/search';
import { searchInputSelector } from '../selectors/search';
import {
  SET_SEARCH_VALUE,
  SELECT_SEARCH_VALUE,
  CLOSE_OPTIONS_LIST,
  SEARCH_CHANGE_VALUE,
} from './constants';
import { errorToast } from '../../services/helper';
import { UNAVAILABLE_GEOLOCATION, DISABLE_GEOLOCATION } from '../../services/errors';

export const searchAction = (data) => ({
  type: SET_SEARCH_VALUE,
  payload: data,
});

export const searchSelectValueAction = (value) => ({
  type: SELECT_SEARCH_VALUE,
  payload: value,
});

export const closeOptionsList = () => ({
  type: CLOSE_OPTIONS_LIST,
});

export const searchChangeValue = (value) => ({
  type: SEARCH_CHANGE_VALUE,
  payload: value,
});

export const findCityByLocation = (successCallback) => (dispatch) => {
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        const {
          data: { Version, Key, Type, Rank, LocalizedName },
        } = await apiSearchCityByLocation(
          `${position.coords.latitude}%2C${position.coords.longitude}`,
        );
        const normilizedData = { Version, Key, Type, Rank, LocalizedName };
        dispatch(searchSelectValueAction(normilizedData));
        successCallback();
      }, () => {
        errorToast(DISABLE_GEOLOCATION);
      });
    } else {
      errorToast(UNAVAILABLE_GEOLOCATION);
    }
    
};

export const findCityFromSearch = () => async (dispatch, getState) => {
  try {
    const state = getState();
    const inputValue = searchInputSelector(state);

    const { data } = await apiSearchCity(inputValue);
    const payload = data === '' ? [] : data;
    dispatch(searchAction(payload));
  } catch (error) {}
};
