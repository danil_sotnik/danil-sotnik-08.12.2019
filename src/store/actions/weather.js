import { searchSelectedSelector } from '../selectors/search';
import { apiGetWeatherForCity, apiGetForecastForCity } from '../../api/weather';
import { beginRequestAction, finishRequestAction } from './settings';
import { GET_WEATHER_CITY, CONVERT_TEMPERATURE } from './constants';


export const getWeatherForCity = (data) => ({
  type: GET_WEATHER_CITY,
  payload: data,
});

export const convertTemperature = (value) => ({
  type: CONVERT_TEMPERATURE,
  payload: value,
});

export const getCityWeather = () => async (dispatch, getState) => {
  try {
    dispatch(beginRequestAction());
    const state = getState();
    const selectedValue = searchSelectedSelector(state);

    const city = await apiGetWeatherForCity(selectedValue.Key);
    const schedule = await apiGetForecastForCity(selectedValue.Key);
    dispatch(
      getWeatherForCity({
        city: { ...city.data[0], name: selectedValue.LocalizedName },
        daily: schedule.data.DailyForecasts,
      }),
    );
    dispatch(finishRequestAction());
  } catch (error) {}
};
