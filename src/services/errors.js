export const DISABLE_GEOLOCATION = 'User denied geolocation prompt or your connection isn\'t secure.';
export const UNAVAILABLE_GEOLOCATION = 'Your browser isn\'t support the Geolocation API.';