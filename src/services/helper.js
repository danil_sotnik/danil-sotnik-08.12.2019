import { NotificationManager } from 'react-notifications';
export const convertISOtoDay = (value) => {
  const day = new Date(value).getDay();
  switch (day) {
    case 0:
      return 'Sunday';
    case 1:
      return 'Monday';
    case 2:
      return 'Tuesday';
    case 3:
      return 'Wednesday';
    case 4:
      return 'Thursday';
    case 5:
      return 'Friday';
    case 6:
      return 'Saturday';
    default:
      return 'Sunday';
  }
};

export const convertFahrenheitToCelsius = (value) =>
  Math.round((5 / 9) * (value - 32));

export const convertCelsiusToFahrenheit = (value) =>
  Math.round((value * 9) / 5 + 32);

export const errorToast = (text) => {
  NotificationManager.error(text, null, 3000)
};