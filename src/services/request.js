import axios from 'axios';
import { store } from '../store/config';
import { setErrorState } from '../store/actions/settings';

const serverUrl = process.env.REACT_APP_API_URL;
const apiCall = axios.create({
  baseURL: serverUrl,
});

apiCall.interceptors.response.use(
  async (response) => {
    store.dispatch(setErrorState(''));
    return response;
  },
  async (error) => {
    const isError = (error && error.request && error.request.response) === '';

    if (isError) {
      store.dispatch(setErrorState("You don't have any free requests today"));
    }

    return Promise.reject(error);
  },
);

export default (requestOptions) => apiCall(requestOptions);
