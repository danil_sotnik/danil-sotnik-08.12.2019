import request from '../services/request';
import { API_AUTOCOMPLETE } from './constants';

export const apiFindCity = (value) =>
  request({
    url: `${API_AUTOCOMPLETE}?apikey=${process.env.REACT_APP_KEY}&language=en-us&q=${value}`,
    method: 'GET',
  });
