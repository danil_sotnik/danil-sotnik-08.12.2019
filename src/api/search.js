import request from '../services/request';
import { API_AUTOCOMPLETE, API_LOCATION } from './constants';

export const apiSearchCity = (value) =>
  request({
    url: `${API_AUTOCOMPLETE}?apikey=${process.env.REACT_APP_KEY}&language=en-us&q=${value}`,
    method: 'GET',
  });

export const apiSearchCityByLocation = (value) =>
  request({
    url: `${API_LOCATION}?apikey=${process.env.REACT_APP_KEY}&language=en-us&q=${value}`,
    method: 'GET',
  });
