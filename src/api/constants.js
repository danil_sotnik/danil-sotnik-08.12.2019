export const API_AUTOCOMPLETE = '/locations/v1/cities/autocomplete';
export const API_CURRENT_CITY = '/currentconditions/v1';
export const API_5DAYS_FORECAST = '/forecasts/v1/daily/5day/';
export const API_LOCATION = '/locations/v1/cities/geoposition/search';
