import request from '../services/request';
import { API_CURRENT_CITY, API_5DAYS_FORECAST } from './constants';

export const apiGetWeatherForCity = (value) =>
  request({
    url: `${API_CURRENT_CITY}/${value}?apikey=${process.env.REACT_APP_KEY}&language=en-us`,
    method: 'GET',
  });

export const apiGetForecastForCity = (value) =>
  request({
    url: `${API_5DAYS_FORECAST}/${value}?apikey=${process.env.REACT_APP_KEY}&language=en-us`,
    method: 'GET',
  });
