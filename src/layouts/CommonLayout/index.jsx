import React from 'react';
import { Header } from '../../components/Header';
import withLoader from '../../hocks/withLoader';
import PropTypes from 'prop-types';
import { NotificationContainer } from 'react-notifications';

export const CommonLayout = withLoader()(({ children }) => {
  return (
    <>
      <Header />
      <div className="App">{children}</div>
      <NotificationContainer />
    </>
  );
});

CommonLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
