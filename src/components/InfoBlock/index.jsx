import React from 'react';
import InfoBlockContainer from './InfoBlockContainer';

export const InfoBlock = ({ children, ...props }) => {
  const { className } = props;
  return (
    <InfoBlockContainer
      {...props}
      className={`info-block ${className ? className : ''}`}
    >
      {children}
    </InfoBlockContainer>
  );
};