import styled from 'styled-components';

export default styled.div`
  background: ${(props) => props.theme.background.infoBackground};
  color: ${(props) => props.theme.colors.textColor};
  display: flex;
  padding: 10px;
  border-radius: 10px;
  justify-content: space-between;
  transition: 0.1s ease-in-out all;
`;
