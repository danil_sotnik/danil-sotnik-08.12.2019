import React from 'react';
import PropTypes from 'prop-types';
import ButtonContainer from './ButtonContainer';
import './button.scss';

export const Button = ({ children, ...props }) => {
  return (
    <ButtonContainer {...props} className="btn">
      {children}
    </ButtonContainer>
  );
};

Button.propTypes = {
  children: PropTypes.element.isRequired,
};
