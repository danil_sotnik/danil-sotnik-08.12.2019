import styled from 'styled-components';

export default styled.button`
  background: ${(props) => props.theme.background.buttonBackground};
  color: ${(props) => props.theme.background.buttonColor};
  transition: 0.1s ease-in-out all;
`;
