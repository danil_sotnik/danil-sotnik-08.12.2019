import styled from 'styled-components';

export default styled.div`
  background: ${(props) => props.theme.background.backgroundHeader};
  color: ${(props) => props.theme.colors.textColor};
  transition: 0.1s ease-in-out all;
  width: 100%;
`;
