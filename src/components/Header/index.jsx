import React from 'react';
import HeaderContainer from './HeaderContainer';
import { useDispatch } from 'react-redux';
import { setThemeAction } from '../../store/actions/settings';
import { StyledNavLink } from './components/Link';
import './header.scss';
import { useSelector } from 'react-redux';
import { themeSelector } from '../../store/selectors/settings';
import { ThemeSwitch } from './components/Switch';
import { Location } from './components/Location';
import { findCityByLocation } from '../../store/actions/search';
import { withRouter } from 'react-router-dom';

export const Header = withRouter (({ history }) => {
  const dispatch = useDispatch();
  const theme = useSelector(themeSelector);

  const toggleTheme = () => {
    const isThemeLight = theme === 'light';
    localStorage.setItem('theme', isThemeLight ? 'dark' : 'light');
    dispatch(setThemeAction());
  };

  const successLocationCallBack = () => history.push('/home');

  const getGeolocation = () => {
    dispatch(findCityByLocation(successLocationCallBack));
  };

  return (
    <HeaderContainer>
      <div className="header">
        <div className="header__title">Weather</div>
        <div className="header__actions">
          <Location onClick={getGeolocation}/>
          <div className="header__actions-nav">
            <StyledNavLink to="/home" activeClassName="selectedLink">
              Home
            </StyledNavLink>
            <StyledNavLink to="/favourites" activeClassName="selectedLink">
              Favourites
            </StyledNavLink>
          </div>
          <ThemeSwitch
            checked={theme === 'light'}
            onChange={toggleTheme}
            classes={{}}
          />
        </div>
      </div>
    </HeaderContainer>
  );
});
