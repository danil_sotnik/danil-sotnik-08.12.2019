import { withStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

export const ThemeSwitch = withStyles({
  switchBase: {
    color: '#67FFC8',
    '&$checked': {
      color: '#5D2BFF',
    },
    '& + $track': {
      backgroundColor: '#67FFC8',
    },
    '&$checked + $track': {
      backgroundColor: '#5D2BFF',
    },
  },
  checked: {},
  track: {},
})(Switch);
