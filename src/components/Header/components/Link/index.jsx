import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const activeClassName = 'active';

export const StyledNavLink = styled(NavLink).attrs({
  activeClassName: activeClassName,
})`
  padding: 5px 15px;
  color: ${(props) => props.theme.colors.textColor};
  text-decoration: none;
  &:hover {
    text-decoration: none;
  }
  &.${activeClassName} {
    color: ${(props) => props.theme.colors.buttonColor};
    border-radius: 5px;
    transition: 0.1s ease-in-out background-color, 0.1s ease-in-out color;
  }
`;
