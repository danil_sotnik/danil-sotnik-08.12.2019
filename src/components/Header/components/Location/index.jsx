import React from 'react';
import styled from 'styled-components';
import { ReactComponent as LocationIcon } from '../../../../icons/location.svg';
const LocationContainer = styled.div`
  cursor: pointer;
  & path {
    fill: ${(props) => props.theme.colors.buttonColor};
  }
`;

export const Location = (props) => {
  return (
    <LocationContainer {...props} >
      <LocationIcon />
    </LocationContainer>
  );
};
