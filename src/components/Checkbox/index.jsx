import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import './checkbox.scss';

const HiddenInput = styled.input.attrs({ type: 'checkbox' })`
  border: 2px solid ${(props) => props.theme.colors.buttonColor};
  background: transparent;
  &::before {
    background: ${(props) => props.theme.colors.buttonColor};
  }
`;
const Label = styled.label`
  color: ${(props) => props.theme.colors.textColor};
`;

export const Checkbox = ({ id, label, ...inputProps }) => (
  <div className="checkbox">
    <Label className="checkbox__label" htmlFor={id}>
      {label}
      <HiddenInput className="checkbox__input" id={id} {...inputProps} />
    </Label>
  </div>
);

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
};

Checkbox.defaultProps = {
  label: '',
};
