import React from 'react';
import InputContainer from './InputContainer';
import './input.scss';
import { OptionsList } from './renderOptions';

export const Input = ({ children, ...props }) => {
  const { options, ...restProps } = props;
  return (
    <div className="home__search">
      <div className="home__search-wrapper">
        <InputContainer className="input-field" {...restProps} />
        <OptionsList options={options} visible={options.length > 0} />
      </div>
    </div>
  );
};
