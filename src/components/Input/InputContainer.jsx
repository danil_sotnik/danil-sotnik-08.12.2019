import styled from 'styled-components';

export default styled.input`
  background: ${(props) => props.theme.background.inputBackground};
  color: ${(props) => props.theme.colors.textColor};
  border: ${(props) => props.theme.border.input};
`;

export const Option = styled.div`
  padding: 7px 15px;
  background: ${(props) => props.theme.background.inputBackground};
  color: ${(props) => props.theme.colors.textColor};
  transition: 0.1s ease-in-out all;
  text-decoration: none;
  cursor: pointer;
  z-index: 9;
  border-left: ${(props) => props.theme.border.input};
  border-right: ${(props) => props.theme.border.input};
  &:hover {
    background: ${(props) => props.theme.hover.inputHoverOptionBackground};
  }
`;
