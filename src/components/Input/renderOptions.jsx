import React from 'react';
import styled from 'styled-components';

const Option = styled.div`
  padding: 5px 15px;
  background: ${(props) => props.theme.colors.inputBackground};
  color: ${(props) => props.theme.colors.textColor};
  transition: 0.1s ease-in-out all;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    background: ${(props) => props.theme.colors.inputHoverOptionBackground};
  }
  &:last-child {
    border-radius: 0 0 15px 15px;
  }
`;

export const OptionsList = ({ options, visible }) => (
  <div className={`input-options ${visible ? 'input-options--show' : ''}`}>
    {options && options.map((value) => <Option>{value}</Option>)}
  </div>
);
