import React from 'react';
import PropTypes from 'prop-types';
import CardContainer from './CardContainer';
import './card.scss';

export const Card = ({ children, ...props }) => {
  const { className } = props;
  return (
    <CardContainer {...props} className={`card ${className ? className : ''}`}>
      {children}
    </CardContainer>
  );
};

Card.propTypes = {
  children: PropTypes.array.isRequired,
};
