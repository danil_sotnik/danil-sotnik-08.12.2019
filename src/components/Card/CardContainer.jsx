import styled from 'styled-components';

export default styled.div`
  background: ${(props) => props.theme.background.cardBackground};
  color: ${(props) => props.theme.colors.textColor};
  box-shadow: ${(props) => props.theme.shadows.card};
  transition: 0.1s ease-in-out all;
`;
