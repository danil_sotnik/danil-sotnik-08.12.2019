import React from 'react';
import PropTypes from 'prop-types';

export const CardHeader = ({ children }) => {
  return <div className="card__header">{children}</div>;
};

CardHeader.propTypes = {
  children: PropTypes.element.isRequired,
};
