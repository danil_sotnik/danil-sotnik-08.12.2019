import React from 'react';
import PropTypes from 'prop-types';

export const CardBody = ({ children }) => {
  return <div className="card__body">{children}</div>;
};

CardBody.propTypes = {
  children: PropTypes.array.isRequired,
};
