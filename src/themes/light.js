export default {
  colors: {
    textColor: '#8E8E8E',
    buttonColor: '#5D2BFF',
    link: '#5E81AC',
    activeLink: '#67FFC8',
    cardColor: '#CFCFD0',
    errorColor: '#fff',
  },
  background: {
    backgroundHeader: '#eaecf0',
    backgroundBody: '#F4F5F7',
    cardBackground: '#ffffff',
    inputBackground: '#ffffff',
    buttonBackground: '#6D44F0',
    infoBackground: '#dddddd',
    loaderBackground: 'rgba(149, 149, 149, 0.9)',
  },
  border: {
    input: '1px solid #bdbdbd',
  },
  hover: {
    inputHoverOptionBackground: '#e2e2e6',
  },
  shadows: {
    card: '0px 5px 20px 0px rgba(212,217,218,0.5)',
  },
};
