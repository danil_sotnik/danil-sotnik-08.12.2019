export default {
  colors: {
    textColor: '#B9B9B9',
    buttonColor: '#67FFC8',
    link: '#c8c8c8',
    activeLink: '#67FFC8',
    cardColor: '#CFCFD0',
    errorColor: '#B9B9B9',
  },
  background: {
    backgroundHeader: '#25262f',
    backgroundBody: '#2c2e38',
    cardBackground: '#373a44',
    inputBackground: '#545766',
    buttonBackground: '#67FFC8',
    infoBackground: '#5c5e66',
    loaderBackground: 'rgba(21, 21, 21, 0.9)',
  },
  border: {
    input: '1px solid #737070',
  },
  hover: {
    inputHoverOptionBackground: '#41414d',
  },
  shadows: {
    info: '-2px 5px 7px -2px rgb(36, 35, 36)',
    card:
      '0 64px 64px 0 rgba(0,0,0,0.1), 0 32px 32px 0 rgba(0,0,0,0.1), 0 16px 16px 0 rgba(0,0,0,0.1), 0 4px 4px 0 rgba(0,0,0,0.1), 0 2px 2px 0 rgba(0,0,0,0.1)',
  },
};
