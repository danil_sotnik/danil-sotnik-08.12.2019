import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { errorSelector } from '../../store/selectors/settings';

export const ErrorBlock = styled.div`
  background: ${(props) => props.theme.background.loaderBackground};
  color: ${(props) => props.theme.colors.errorColor};
  min-height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  padding: 10px;
`;

export default () => (Component) => {
  const WithServerErrorHandling = (props) => {
    const error = useSelector(errorSelector);
    return (
      <>
        {error ? (
          <ErrorBlock>
            Unfortunately, you trial time is over. You can continue it tomorrow.
          </ErrorBlock>
        ) : (
          <Component {...props} />
        )}
      </>
    );
  };

  WithServerErrorHandling.propTypes = {
    error: PropTypes.bool,
  };

  WithServerErrorHandling.defaultProps = {
    error: false,
  };

  return WithServerErrorHandling;
};
