import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { ReactComponent as Loader } from '../../icons/loader.svg';
import './style.scss';

export const LoaderBack = styled.div`
  background: ${(props) => props.theme.background.loaderBackground};
`;

export default () => (Component) => {
  const WithProcessingIndicator = ({ isLoading, ...props }) => {
    const className = `progress-indicator ${isLoading ? '__active' : ''}`;
    const isContentActive = isLoading ? '__active' : '';
    return (
      <>
        <Component className={isContentActive} {...props} />
        <div className={className}>
          <Loader />
          <LoaderBack className="progress-indicator__back" />
        </div>
      </>
    );
  };

  WithProcessingIndicator.propTypes = {
    isLoading: PropTypes.bool,
  };

  WithProcessingIndicator.defaultProps = {
    isLoading: false,
  };

  return WithProcessingIndicator;
};
