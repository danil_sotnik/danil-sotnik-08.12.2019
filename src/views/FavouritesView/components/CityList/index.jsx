import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './citylist.scss';
import { InfoBlock } from '../../../../components/InfoBlock';
import { favouritesSelector } from '../../../../store/selectors/favourites';
import { searchSelectValueAction } from '../../../../store/actions/search';
import { getAllFavouritesWeather } from '../../../../store/actions/favourites';
import PropTypes from 'prop-types';
import { Card } from '../../../../components/Card';

export const CityList = withRouter(({ history }) => {
  const favouritesCities = useSelector(favouritesSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllFavouritesWeather());
  }, [dispatch]);

  const clickHandler = (value) => {
    dispatch(searchSelectValueAction(value));
    history.push('/home');
  };

  return (
    <>
      {favouritesCities.length > 0 ? (
        favouritesCities.map((city, i) => (
          <InfoBlock
            key={i}
            className="info-block--city"
            onClick={() => clickHandler(city)}
          >
            <div className="info-block__name">{city.LocalizedName}</div>
            <div className="info-block__temp">{city.temp}°F</div>
            <div className="info-block__weather">{city.weather}</div>
          </InfoBlock>
        ))
      ) : (
        <div className="info-block--no-content">
          <Card>You haven't had any favourite cities yet</Card>
        </div>
      )}
    </>
  );
});

CityList.propTypes = {
  history: PropTypes.object,
};
