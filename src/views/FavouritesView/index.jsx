import React from 'react';
import FavouritesContainer from './FavouritesContainer';
import { CityList } from './components/CityList';

import './favourite.scss';
import withError from '../../hocks/withError';

const FavouriteViewComponent = () => {
  return (
    <FavouritesContainer className="city-block">
      <div className="city-block__wrapper">
        <CityList />
      </div>
    </FavouritesContainer>
  );
};

export const FavouriteView = withError()(FavouriteViewComponent);
