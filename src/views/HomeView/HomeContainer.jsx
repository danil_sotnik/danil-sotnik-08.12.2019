import styled from 'styled-components';

export default styled.div`
  background: ${(props) => props.theme.background.backgroundBody};
  color: ${(props) => props.theme.colors.textColor};
  min-height: 100%;
  transition: 0.1s ease-in-out all;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
`;
