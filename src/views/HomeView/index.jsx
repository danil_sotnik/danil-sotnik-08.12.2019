import React from 'react';
import HomeContainer from './HomeContainer';
import { Search } from './components/Search';
import './home.scss';
import { Info } from './components/Info';
import withError from '../../hocks/withError';

const HomeViewComponent = () => {
  return (
    <div className="home">
      <HomeContainer>
        <Search />
        <Info isLoading={true} />
      </HomeContainer>
    </div>
  );
};
export const HomeView = withError()(HomeViewComponent);
