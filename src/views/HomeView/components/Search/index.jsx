import React, { useEffect, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import debounce from 'lodash/debounce';
import Autosuggest from 'react-autosuggest';
import {
  searchSelector,
  searchInputSelector,
} from '../../../../store/selectors/search';
import {
  findCityFromSearch,
  searchSelectValueAction,
  searchChangeValue,
  closeOptionsList,
} from '../../../../store/actions/search';
import InputContainer, {
  Option,
} from '../../../../components/Input/InputContainer';
import './search.scss';

export const Search = () => {
  const dispatch = useDispatch();
  const options = useSelector(searchSelector);
  const searchValue = useSelector(searchInputSelector);
  const [isOpenList, setOpenList] = useState(false);

  const updateData = useCallback(
    debounce(() => dispatch(findCityFromSearch()), 200),
    [dispatch],
  );

  useEffect(() => {
    if (isOpenList) {
      updateData();
    }
  }, [isOpenList, searchValue, updateData]);

  const onChange = (event, { newValue }) => {
    setOpenList(true);
    dispatch(searchChangeValue(newValue));
  };

  const getSuggestionValue = (suggestion) => suggestion.LocalizedName;

  const renderSuggestion = (suggestion) => (
    <Option>{suggestion.LocalizedName}</Option>
  );

  const inputProps = {
    placeholder: 'Type city name',
    value: searchValue,
    onChange: onChange,
  };

  return (
    <>
      <Autosuggest
        suggestions={options}
        onSuggestionsFetchRequested={() => options}
        onSuggestionSelected={(e, { suggestion }) => {
          dispatch(searchSelectValueAction(suggestion));
          setOpenList(false);
        }}
        onSuggestionsClearRequested={() => dispatch(closeOptionsList())}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        renderInputComponent={(inputProps) => (
          <InputContainer {...inputProps} className="home__search-field" />
        )}
        inputProps={inputProps}
      />
    </>
  );
};
