import React, { useEffect, useState } from 'react';
import { Card } from '../../../../components/Card';
import { CardHeader } from '../../../../components/Card/components/CardHeader';
import { CardBody } from '../../../../components/Card/components/CardBody';
import { InfoBlock } from '../../../../components/InfoBlock';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCityWeather,
  convertTemperature,
} from '../../../../store/actions/weather';
import {
  weatherCitySelector,
  weatherForecastSelector,
  isCelsiusSelector,
} from '../../../../store/selectors/weather';
import { searchSelectedSelector } from '../../../../store/selectors/search';
import { Checkbox } from '../../../../components/Checkbox';
import { LikeButton } from './components/LikeButton';
import { changeFavouriteStatusAction } from '../../../../store/actions/favourites';
import './info.scss';
import { favouritesSelector } from '../../../../store/selectors/favourites';

export const InfoComponent = () => {
  const dispatch = useDispatch();
  const cityWeather = useSelector(weatherCitySelector);
  const dailyWeather = useSelector(weatherForecastSelector);
  const selectedValue = useSelector(searchSelectedSelector);
  const isCelsius = useSelector(isCelsiusSelector);
  const favourites = useSelector(favouritesSelector);
  const [value, setValue] = useState(isCelsius);

  useEffect(() => {
    setValue(isCelsius);
  }, [isCelsius]);

  const isFavourite =
    favourites.filter((item) => item.Key === selectedValue.Key).length !== 0;

  useEffect(() => {
    dispatch(getCityWeather());
  }, [dispatch, selectedValue]);

  const convertTemperatureValue = () => {
    dispatch(convertTemperature(!value));
  };

  const handleFavourite = () => {
    dispatch(changeFavouriteStatusAction());
  };

  const renderCardHeader = () => (
    <CardHeader>
      <div className="card__header-wrapper">
        <div className="card__info">
          <div className="card__image">&#x2327;</div>
          <div className="card__city">
            <div className="card__city-name">{cityWeather.city}</div>
            <div className="card__city-temp">{`${cityWeather.temp}°${
              isCelsius ? 'C' : 'F'
            }`}</div>
          </div>
        </div>
        <div className="card__actions">
          <Checkbox
            id="convert"
            label="Convert to Celsius"
            checked={value}
            onChange={convertTemperatureValue}
          />
          <LikeButton active={isFavourite} onClick={handleFavourite} />
        </div>
      </div>
    </CardHeader>
  );

  const renderCardBody = () => (
    <CardBody>
      <div className="card__title">{cityWeather.weather}</div>
      <div className="card__schedule">
        {dailyWeather.map((item, i) => (
          <InfoBlock key={i} className="info-block--day">
            <div className="card__schedule-item">
              <div className="card__schedule-day">{item.day}</div>
              <div className="card__values">
                <div className="card__temp">
                  <div className="card__temp-range">min</div>
                  <div className="card__temp-value">{`${item.temp.min}°${
                    isCelsius ? 'C' : 'F'
                  }`}</div>
                </div>
                <div className="card__temp">
                  <div className="card__temp-range">max</div>
                  <div className="card__temp-value">{`${item.temp.max}°${
                    isCelsius ? 'C' : 'F'
                  }`}</div>
                </div>
              </div>
            </div>
          </InfoBlock>
        ))}
      </div>
    </CardBody>
  );

  return (
    <div className="card-wrapper">
      {cityWeather && dailyWeather && (
        <Card className="card--city">
          {renderCardHeader()}
          {renderCardBody()}
        </Card>
      )}
    </div>
  );
};

export const Info = InfoComponent;
