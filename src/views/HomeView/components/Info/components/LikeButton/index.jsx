import React from 'react';
import { ReactComponent as Like } from '../../../../../../icons/like.svg';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const LikeContainer = styled.div`
  height: 30px;
  width: 30px;
  & path {
    fill: grey;
    transition: 0.2s ease-in-out all;
    cursor: pointer;
  }
  &.like-btn--active path {
    fill: ${(props) => props.theme.colors.buttonColor};
  }
`;

export const LikeButton = ({ active, ...props }) => {
  return (
    <LikeContainer
      className={`like-btn ${active ? 'like-btn--active' : ''}`}
      {...props}
    >
      <Like />
    </LikeContainer>
  );
};

LikeButton.propTypes = {
  active: PropTypes.bool,
};

LikeButton.defaultProps = {
  active: false,
};
