import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { HomeView } from './views/HomeView';
import { CommonLayout } from './layouts/CommonLayout';
import { ThemeProvider } from 'styled-components';
import lightTheme from './themes/light';
import darkTheme from './themes/dark';
import { useSelector } from 'react-redux';
import { themeSelector, loadingSelector } from './store/selectors/settings';
import { FavouriteView } from './views/FavouritesView';
import 'react-notifications/lib/notifications.css';

const App = () => {
  const theme = useSelector(themeSelector);
  const isLoading = useSelector(loadingSelector);
  const isLightTheme = theme === 'light';

  return (
    <ThemeProvider theme={isLightTheme ? lightTheme : darkTheme}>
      <CommonLayout isLoading={isLoading}>
        <Switch>
          <Route exact path="/home" component={HomeView} />
          <Route exact path="/favourites" component={FavouriteView} />
          <Redirect from="/" to="/home" />
          <Route exact component={() => <div>404</div>} />
        </Switch>
      </CommonLayout>
    </ThemeProvider>
  );
};

export default App;
